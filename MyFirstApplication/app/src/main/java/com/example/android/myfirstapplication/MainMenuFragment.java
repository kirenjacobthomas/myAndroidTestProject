package com.example.android.myfirstapplication;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainMenuFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    String[] mainMenuArray = {
            "Raphael",
            "Leonardo",
            "Michelangelo",
            "Donatello",
            "Picasso",
            "Rembrandt",
            "Monet"
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);

        // Inflate the layout for this fragment
        List<String> mainMenuList = new ArrayList<String>(Arrays.asList(mainMenuArray));

        ArrayAdapter<String> menuArrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item_menu,
                R.id.list_item_menu_textview,
                mainMenuList);

        final ListView mainMenuListView = (ListView) rootView.findViewById(R.id.menu_listView);
        mainMenuListView.setAdapter(menuArrayAdapter);

        mainMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getActivity().getApplicationContext();
                String toastTxt = ((TextView)view.findViewById(R.id.list_item_menu_textview)).getText().toString();
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, toastTxt, duration);
                toast.show();

                mListener.onFragmentInteraction(position);

            }
        });

        return rootView;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int id);
    }
}
