package com.example.android.myfirstapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.Random;


public class MainActivity extends AppCompatActivity implements MainMenuFragment.OnFragmentInteractionListener {

    public static final String EXTRA_MESSAGE = "com.example.android.myfirstapplication";

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        MainMenuFragment menuFragment = new MainMenuFragment();
//        menuFragment.SetOnFragmentInteractionListener(this);
//        fragmentManager = getSupportFragmentManager();
//        fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.fragmentHolder, menuFragment);
//        fragmentTransaction.commit();

    }

    public void BtnListener(View view) {

        Toast.makeText(this,"Btn Pressed!!!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onFragmentInteraction(int id)
    {
        int picId = (new Random().nextInt(4)) + 1;


        fragmentManager = getSupportFragmentManager();
        if(fragmentManager.findFragmentById(R.id.naturalFragmentHolder)!= null){
            ((NaturalFragment)fragmentManager.findFragmentById(R.id.naturalFragmentHolder)).ChangeImgSource("@drawable/butterfly_" + picId);
        }

    }
}
