package com.example.android.myfirstapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class NaturalFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_natural, container, false);
    }

    public void ChangeImgSource(String imgSrc) {
        try {
            ImageView imageView = (ImageView) getView().findViewById(R.id.butterflyView);
            imageView.setImageResource(getResources().getIdentifier( imgSrc, "drawable", "com.example.android.myfirstapplication"));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
